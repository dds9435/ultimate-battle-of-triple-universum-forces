extends Node2D

onready var tween_out = $"Tween"

export var transition_type = Tween.TRANS_SINE #1

func _ready():
	$"CanvasModulate".visible = true
	$"Anim_Clouds".play("clouds")
	$"Anim_Sun".play("sun")
	$"Anim_Mult".play("mult")
	fade_out($"AudioStreamPlayer".get_path(), -80, -20, 2)
	$"AudioStreamPlayer".play(8)


func to_credits():
	get_tree().change_scene("res://assets/scenes/end_credits.tscn")


func fade_out(path_stream_player, start_vol, end_vol, duration=1.0):
	var stream_player = get_node(path_stream_player)
	# tween music volume down to 0
	tween_out.interpolate_property(
		stream_player, 
		"volume_db", 
		start_vol, end_vol, 
		duration, transition_type, Tween.EASE_IN, 0
	)
	tween_out.start()
	# when the tween ends, the music will be stopped


func _on_TweenOut_tween_completed(object, key):
	# stop the music -- otherwise it continues to run at silent volume
	object.stop()
